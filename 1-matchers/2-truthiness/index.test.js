import { sum } from '../1-basic/index';

beforeEach(() => {
  expect.hasAssertions();
});

test('demo', () => {
  const result = null;

  expect(result).toBeNull();
});

test('result被定义', () => {
  const result = null;

  // <--start
  // TODO: 给出正确的assertion
  expect(result).toBeNull();
  // --end->
});

test('匹配result为真', () => {
  const result = sum(1, 2) === 3;

  // <--start
  // TODO: 给出正确的assertion
  expect(result).toBeTruthy();
  // --end->
});
